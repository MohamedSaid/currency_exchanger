import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'currency_exchanger' },
  {
    path: 'currency_exchanger',
    loadChildren: () =>
      import(
        'src/app/modules/currency_exchanger/currency_exchanger.module'
      ).then((m) => m.CurrencyExchangerModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
