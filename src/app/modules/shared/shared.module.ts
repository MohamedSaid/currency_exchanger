import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './components/header/header.component';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [HeaderComponent],
  imports: [CommonModule, FormsModule, TranslateModule, RouterModule],
  exports: [
    HeaderComponent,
    CommonModule,
    TranslateModule,
    FormsModule,
    RouterModule,
  ],
  providers: [],
})
export class SharedModule {}
