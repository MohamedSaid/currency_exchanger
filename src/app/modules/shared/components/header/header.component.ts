import { Component, OnInit } from '@angular/core';
import { TranslatePipe } from '@ngx-translate/core';
import { Currency } from 'src/app/modules/currency_exchanger/models/enums';

@Component({
  selector: 'bm-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  providers: [TranslatePipe],
})
export class HeaderComponent implements OnInit {
  Currency = Currency;

  ngOnInit(): void {
    /**
     * This is intentional
     */
  }
}
