import { TestBed } from '@angular/core/testing';

import { XhrService } from './xhr.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('XhrService', () => {
  let service: XhrService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TranslateModule.forRoot()],

    });
    service = TestBed.inject(XhrService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
