import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { map } from 'rxjs';
import { environment } from 'src/environments/environment';

interface Config {
  url?: string;
  body?: any;
  params?: {
    [param: string]: string | number | boolean | (string | number | boolean)[];
  };
}

interface API_Response<T> {
  Data: T;
  Message: string;
  Status: number;
}

@Injectable({
  providedIn: 'root',
})
export class XhrService {
  private domainURL = environment.backendURL;
  private API_KEY = environment.API_KEY;
  private http = inject(HttpClient);

  get<T>(API_Config: Config) {
    if (API_Config.params) API_Config.params['access_key'] = this.API_KEY;
    return this.http
      .get<API_Response<T>>(`${this.domainURL}${API_Config.url}`, {
        params: API_Config.params,
      })
      .pipe(
        map((event) => {
          return event;
        })
      );
  }

  post<T>(API_Config: Config) {
    if (API_Config.params) API_Config.params['access_key'] = this.API_KEY;

    return this.http
      .post<API_Response<T>>(
        `${this.domainURL}${API_Config.url}`,
        API_Config.body,
        {
          params: API_Config.params,
        }
      )
      .pipe(
        map((event) => {
          return event;
        })
      );
  }
}
