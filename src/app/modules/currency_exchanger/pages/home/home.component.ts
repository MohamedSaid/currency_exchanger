import { Component, OnInit } from '@angular/core';
import { CurrencyExchangerService } from '../../services/currency-exchanger.service';
import { IConvertObj } from '../../models/interfaces';

@Component({
  selector: 'bm-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  convertObj!: IConvertObj;
  pupilarListCurrency: IConvertObj[] = [];
  constructor(private currencyExchangerService: CurrencyExchangerService) {}

  ngOnInit(): void {}
   onConvert(convertObj: IConvertObj): void {
    this.convertObj = convertObj;
    this.buildPupilarListCurrency();
  }

  buildPupilarListCurrency(): void {
    this.currencyExchangerService
      .convertToPupilarCurrency(this.convertObj.value!, this.convertObj.from)
      .subscribe((res) => {
        this.pupilarListCurrency = res.map((etch: number) => ({
          from: this.convertObj.from,
          to: this.convertObj.to,
          value: this.convertObj.value,
          amount: etch,
        }));
      });
  }
}
