import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { CurrencyExchangerService } from '../../services/currency-exchanger.service';
import { of } from 'rxjs';
import { IConvertObj } from '../../models/interfaces';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let currencyExchangerService: CurrencyExchangerService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      imports: [
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        RouterTestingModule,
      ],
    })
    .compileComponents();

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    currencyExchangerService = TestBed.inject(CurrencyExchangerService);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should build pupilar list of currency correctly', () => {
    // Mock the necessary data and dependencies
    const convertObj = {
      value: 100,
      from: 'USD',
      to: 'EUR',
    } as IConvertObj;
    const mockResponse = [1.2, 1.3, 1.4];
  
    spyOn(currencyExchangerService, 'convertToPupilarCurrency').and.returnValue(
      of(mockResponse)
    );
  
    // Call the method
    component.convertObj = convertObj;
    component.buildPupilarListCurrency();
  
    // Verify the result
    expect(component.pupilarListCurrency).toEqual([
      { from: 'USD', to: 'EUR', value: 100, amount: 1.2 },
      { from: 'USD', to: 'EUR', value: 100, amount: 1.3 },
      { from: 'USD', to: 'EUR', value: 100, amount: 1.4 },
    ]);
  });
});
