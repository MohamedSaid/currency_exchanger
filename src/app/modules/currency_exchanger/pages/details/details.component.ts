import { Component, OnInit } from '@angular/core';
import { IConvertObj } from '../../models/interfaces';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CurrencyExchangerService } from '../../services/currency-exchanger.service';

@Component({
  selector: 'bm-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {
  convertObj: IConvertObj = {} as IConvertObj;
  currenciesData: any[] = [];
  constructor(
    private route: ActivatedRoute,
    private currencyExchangerService: CurrencyExchangerService
  ) {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.convertObj.from = params.get('from') || '';
      this.convertObj.to = params.get('to') || '';
      this.convertObj.value = +params.get('value')! || null;
    });
  }

  ngOnInit(): void {
    this.getHorizontalTimeSeries();
  }
  getHorizontalTimeSeries(): void {
    this.currencyExchangerService
      .getHorizontalTimeSeries('', '')
      .subscribe((res) => {
        this.currenciesData = res;
      });
  }

  onConvert(convertObj: IConvertObj): void {
    this.convertObj = convertObj;
  }
}
