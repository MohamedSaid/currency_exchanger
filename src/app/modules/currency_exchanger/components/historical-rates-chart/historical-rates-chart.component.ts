import { Component, Input, OnInit, ViewChild } from '@angular/core';
import * as Highcharts from 'highcharts';
@Component({
  selector: 'bm-historical-rates-chart',
  templateUrl: './historical-rates-chart.component.html',
  styleUrls: ['./historical-rates-chart.component.scss'],
})
export class HistoricalRatesChartComponent implements OnInit {
  @Input() currenciesData: any[] = [];

  ngOnInit(): void {}
  @ViewChild('chart') chartRef: any;
  chart: Highcharts.Chart | undefined;

  ngAfterViewInit() {
    this.initializeChart();
  }

  initializeChart() {
    this.chart = Highcharts.chart(this.chartRef.nativeElement, {
      title: {
        text: 'Monthly Historical Rates',
      },
      xAxis: {
        categories: this.getMonths(),
      },
      yAxis: {
        title: {
          text: 'Rates',
        },
      },
      series: this.getSeriesData(),
    });
  }

  getMonths() {
    // Logic to get the months for the past year
    // Example logic:
    const months = [];
    const currentDate = new Date();
    for (let i = 11; i >= 0; i--) {
      const date = new Date(
        currentDate.getFullYear(),
        currentDate.getMonth() - i,
        1
      );
      months.push(date.toLocaleString('default', { month: 'long' }));
    }
    return months;
  }

  getSeriesData() {
    // Logic to calculate monthly rates based on the last day of each month
    // Example logic:
    const series: any = [];
    this.currenciesData.forEach((currency) => {
      const monthlyRates = [];
      for (let i = 11; i >= 0; i--) {
        const date = new Date();
        date.setFullYear(date.getFullYear(), date.getMonth() - i + 1, 0);
        const rate = currency.rates[date.toISOString().substring(0, 10)];
        monthlyRates.push(rate);
      }
      series.push({
        name: currency.name,
        data: monthlyRates,
      });
    });
    return series;
  }
}
