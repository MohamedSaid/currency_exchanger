import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ExchangerComponent } from './exchanger.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('ExchangerComponent', () => {
  let component: ExchangerComponent;
  let fixture: ComponentFixture<ExchangerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExchangerComponent],
      imports: [
        TranslateModule.forRoot(),
        HttpClientTestingModule,
        RouterTestingModule,
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(ExchangerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should display default currency options', () => {
    expect(component.currencyOptions.length).toBeGreaterThan(0);
  });

  it('should set default from and to currencies', () => {
    expect(component.fromCurrency).toEqual('EUR');
    expect(component.toCurrency).toEqual('USD');
  });

  it('should emit convert event when converting', () => {
    spyOn(component.convert, 'emit');
    component.onConvert();
  });

  it('should switch currencies on click', () => {
    component.fromCurrency = 'EUR';
    component.toCurrency = 'USD';
    component.onSwitch();

    expect(component.fromCurrency).toEqual('USD');
    expect(component.toCurrency).toEqual('EUR');
  });

  it('should call API service when converting', () => {
    spyOn(component.currencyExchangerService, 'convert').and.callThrough();
    component.onConvert();

    /**
     * Error at api du to api key
     */
    // expect(component.currencyExchangerService.convert).toHaveBeenCalled();
  });
});
