import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CurrencyExchangerService } from '../../services/currency-exchanger.service';
import { Currency } from '../../models/enums';
import { IConvertObj } from '../../models/interfaces';

@Component({
  selector: 'bm-exchanger',
  templateUrl: './exchanger.component.html',
  styleUrls: ['./exchanger.component.scss'],
})
export class ExchangerComponent implements OnInit {
  currencyOptions: string[] = [];
  @Input() fromCurrency: string = 'EUR';
  @Input() toCurrency: string = 'USD';
  @Input() value!: number;

  @Output() convert: EventEmitter<IConvertObj> = new EventEmitter();

  constructor(public currencyExchangerService: CurrencyExchangerService) {}

  ngOnInit(): void {
    this.getCurrencyOptions();
  }

  getCurrencyOptions(): void {
    this.currencyOptions = Object.entries(Currency).map(
      ([key, value]) => value
    );
  }

  onSwitch(): void {
    [this.fromCurrency, this.toCurrency] = [this.toCurrency, this.fromCurrency];

    this.onConvert();
  }

  onConvert(): void {
    if (this.value)
      this.currencyExchangerService
        .convert(this.fromCurrency, this.toCurrency, this.value)
        .subscribe((res) => {
          this.sendValue(res);
        });
  }

  sendValue(amount: number): void {
    const convertObj: IConvertObj = {
      from: this.fromCurrency,
      to: this.toCurrency,
      value: this.value,
      amount,
    };

    this.convert.emit(convertObj);
  }
}
