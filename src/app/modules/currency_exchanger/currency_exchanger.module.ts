import { NgModule } from '@angular/core';

import { HomeComponent } from './pages/home/home.component';
import { DetailsComponent } from './pages/details/details.component';
import { CurrencyExchangerRoutingModule } from './currency_exchanger-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ExchangerComponent } from './components/exchanger/exchanger.component';
import { HistoricalRatesChartComponent } from './components/historical-rates-chart/historical-rates-chart.component';

@NgModule({
  declarations: [HomeComponent, DetailsComponent, ExchangerComponent, HistoricalRatesChartComponent],
  imports: [CurrencyExchangerRoutingModule, SharedModule],
  providers: [],
})
export class CurrencyExchangerModule {}
