export enum Currency {
  EUR = 'EUR',
  AUD = 'AUD',
  CAD = 'CAD',
  CHF = 'CHF',
  CNY = 'CNY',
  GBP = 'GBP',
  JPY = 'JPY',
  USD = 'USD',
}
