export interface IConvertObj {
  from: string;
  to: string;
  value: number | null;
  amount: number;
}
