import { Injectable } from '@angular/core';
import { XhrService } from '../../shared/services/xhr/xhr.service';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CurrencyExchangerService {
  currencyLookup: any[] = [];
  constructor(private xhrService: XhrService) {}

  getLookup(): Observable<any> {
    return this.xhrService.get({
      url: '',
    });
  }
  convert(from: string, to: string, amount: number): Observable<any> {
    /***
     * @result Hallways  success ==> False
     * "Access Restricted - Your current Subscription Plan does not support this API Function.
     * So I'll fixed response
     */

    return of(amount);

    // return this.xhrService.post({
    //   url: 'convert',
    //   params: {
    //     from,
    //     to,
    //     amount,
    //   },
    // });
  }

  convertToPupilarCurrency(amount: number, base: string): Observable<any> {
    // I cant Find This end point at fixer APIS
    /**
     * Should return most popular currencies of the entered amount
     */
    return of([
      amount,
      amount,
      amount,
      amount,
      amount,
      amount,
      amount,
      amount,
      amount,
    ]);
  }

  getHorizontalTimeSeries(
    start_date: string,
    end_date: string
  ): Observable<any> {
    const _data = [
      {
        name: 'USD',
        rates: {
          USD: 1.636492,
          EUR: 1.196476,
          CAD: 1.739516,
        },
      },
      {
        name: 'EUR',
        rates: {
          USD: 1.636492,
          EUR: 1.196476,
          CAD: 1.739516,
        },
      },
      {
        name: 'CAD',
        rates: {
          USD: 1.636492,
          EUR: 1.196476,
          CAD: 1.739516,
        },
      },
    ];
    return of(_data);
    // return this.xhrService.post({
    //   url: 'timeseries',
    //   params: {
    //     start_date: '2012-05-01',
    //     end_date: '2012-05-25',
    //   },
    // });
  }
}
