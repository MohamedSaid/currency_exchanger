import { TestBed } from '@angular/core/testing';

import { CurrencyExchangerService } from './currency-exchanger.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('CurrencyExchangerService', () => {
  let service: CurrencyExchangerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, TranslateModule.forRoot()],
    });
    service = TestBed.inject(CurrencyExchangerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
